module Main where

import Test.Tasty
import qualified Properties

main :: IO ()
main = defaultMain $ testGroup "Tests" [Properties.tests]

