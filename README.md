bytestring-from
===============

A type-class to convert values from `ByteString`.
